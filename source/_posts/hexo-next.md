---
title: Hexo & NexT
categories:
- 工具
tags: 
- Hexo
---

## Hexo

### 新特性

Hexo 版本 5.0 之后，提供了主题配置文件 _config.[name].yml。

现在可以使用 _config.next.yml 来管理 NexT 主题配置了，赞！

### Hexo Server

笔者使用的 Hexo 版本是 5.4.0。

安装 Hexo 后，执行 `hexo s` 命令，Hexo 不会监视文件变动并自动更新，须重启服务器。

参阅 [Hexo 服务器](https://hexo.io/zh-cn/docs/server.html)，可知：Hexo 3.0 后，须安装 hexo-server。

```bash
npm install hexo-server --save
```

### GitLab Pages

使用 GitLab CI 将 Hexo 博客部署到 GitLab Pages 上。

参阅 [将 Hexo 部署到 GitLab Pages](https://hexo.io/zh-cn/docs/gitlab-pages)

## NexT

提供了 npm 安装，赞！

### 标签 & 分类 & 关于

```bash
hexo new page tags
hexo new page categories
hexo new page about
```

## 写作

```bash
hexo new draft hexo-next
hexo publish hexo-next
```



