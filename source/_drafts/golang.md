---
title: Go 程序设计语言
date: 2021-04-14 09:00:45
category: 
- 编程语言
tags:
- Go
---

优势：
- 部署简单(二进制、无依赖、直接运行) 
- 静态类型（编译器检查）
- 原生并发支持
- 强大的标准库（runtime 系统调度、高效的 GC、丰富的标准库——
- 简单易学（25 个关键字、内嵌 C 语法、面向对象特征、跨平台）
- 大厂领军

强项：
- 云计算基础实施（docker、k8s、etcd、consul）
- 基础后端软件（tidb）
- 微服务（go-kit、mirco）
- 互联网基础设施（以太坊）

成就：
- docker
- k8s

劣势：
- 包管理，托管在 github
- 无泛化类型
- Exception 使用 Error 处理
- c 的降级处理并非无缝


## Go 环境

Linux(Debian 10.0)

### 安装

```bash
sudo apt install go
```

使用 gvm 安装&管理 go 的版本。

### 配置环境变量

```bash
go env

```


### 确认

```bash
go version
go --help
```

## 示例

```go
package main

import "fmt"

func main () {
  fmt.Println("Hello, world")
}

```

